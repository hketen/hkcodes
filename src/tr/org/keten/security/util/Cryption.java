package tr.org.keten.security.util;

public class Cryption {

	public static String encrypt(String s, char c) {
		return sifrele(s, c);
	}

	public static String decrypt(String s, char c) {
		return coz(s, c);
	}

	private static String sifrele(String s, char c) {
		char[] cs = s.toCharArray();

		int ic = c;
		if (ic % 2 == 0) {
			ic /= 2;
			ic-=3;
		} else {
			ic *= 2;
			ic-=2;
		}

		String str = "";

		for (int i = 0; i < cs.length; i++) {
			str += (char) (cs[i] * ic);
		}

		return str;
	}

	private static String coz(String s, char c) {
		char[] cs = s.toCharArray();

		int ic = c;
		if (ic % 2 == 0) {
			ic /= 2;
			ic-=3;
		} else {
			ic *= 2;
			ic-=2;
		}
		
		String str = "";

		for (int i = 0; i < cs.length; i++) {
			str += (char) (cs[i] / ic);
		}

		return str;
	}
}