package tr.org.keten.security.test;

import tr.org.keten.security.util.Cryption;

public class CryptionDemo {

	public static void main(String[] args) {
		String sifreli = Cryption.encrypt("Hakan", 'a');
		System.out.println("Gönderilen: Hakan -> Gelen: " + sifreli);
		String cozulen = Cryption.decrypt(sifreli, 'a');
		System.out.println("Gönderilen: " + sifreli + " -> Gelen: " + cozulen);
	}
}