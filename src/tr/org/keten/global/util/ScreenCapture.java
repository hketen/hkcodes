package tr.org.keten.global.util;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

public class ScreenCapture {

	/**
	 * @Using: String saveDictionary;
	 * 
	 *         {@code
	 * saveDictionary = "./Capture/image.jpg";
	 * or 
	 * saveDictionary = "./Capture/" + System.currentTimeMillis() + ".jpg";
	 * } ScreenCapture.capture(saveDictionary);
	 * 
	 * @return saved file
	 * */
	public static File capture(String saveDictionary) {
		File file = null;

		try {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			BufferedImage capture = new Robot()
					.createScreenCapture(new Rectangle(screenSize));

			file = new File(saveDictionary);

			if (!file.isAbsolute()) {
				file.mkdirs();
			}

			ImageIO.write(capture, "jpg", file);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return file;
	}
}
