package tr.org.keten.commandpromp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CommandPromp {
	public static String exec(String command) {
		String output = "";

		try {
			Process p = Runtime.getRuntime().exec(command);

			p.waitFor();
			BufferedReader buf = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			String line = "";

			while ((line = buf.readLine()) != null) {
				output += line + "\n";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;
	}
}
