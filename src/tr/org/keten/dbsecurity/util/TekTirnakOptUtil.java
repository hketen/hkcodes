package tr.org.keten.dbsecurity.util;

public class TekTirnakOptUtil {
	public static String sqlString(String string) {
		String ret = "";

		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) == '\'') {
				ret += "\\" + string.charAt(i);
			} else {
				ret += string.charAt(i);
			}
		}
		return ret;
	}
}
