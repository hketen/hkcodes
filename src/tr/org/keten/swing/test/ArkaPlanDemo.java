package tr.org.keten.swing.test;

import javax.swing.JFrame;

import tr.org.keten.swing.components.HkArkaPlanPanel;

public class ArkaPlanDemo extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ArkaPlanDemo() {
		init();
	}

	private void init() {
		add(initPanel());

		setSize(800, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

	private HkArkaPlanPanel initPanel() {
		HkArkaPlanPanel panel = new HkArkaPlanPanel("./img/test.jpg", false);

		return panel;
	}

	public static void main(String[] args) {
		new ArkaPlanDemo();
	}
}
