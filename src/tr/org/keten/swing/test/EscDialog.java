package tr.org.keten.swing.test;

import java.awt.event.KeyEvent;

import javax.swing.JDialog;
import javax.swing.KeyStroke;

import tr.org.keten.swing.util.HkWindowUtil;

public class EscDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	public EscDialog() {
		HkWindowUtil.initCloseListener(this,
				KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
		setSize(300, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public static void main(String[] args) {
		new EscDialog();
	}
}