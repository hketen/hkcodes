package tr.org.keten.swing.test;

import javax.swing.JFrame;

import tr.org.keten.swing.components.HkTextField;

public class HkTextFieldDemo {

	public static void main(String[] args) {

		JFrame f = new JFrame();

		HkTextField field = new HkTextField("Adınızı Giriniz");
		f.add(field);
		
		f.pack();
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}

}
