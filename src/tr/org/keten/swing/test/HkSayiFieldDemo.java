package tr.org.keten.swing.test;

import javax.swing.JFrame;

import tr.org.keten.swing.components.HkSayiField;

public class HkSayiFieldDemo {

	public static void main(String[] args) {

		JFrame f = new JFrame();

		HkSayiField field = new HkSayiField(20);
		f.add(field);

		f.pack();
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}

}
