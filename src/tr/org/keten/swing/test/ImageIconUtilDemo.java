package tr.org.keten.swing.test;

import javax.swing.ImageIcon;

import tr.org.keten.swing.util.ImageIconUtil;

public class ImageIconUtilDemo {

	public ImageIconUtilDemo() {
		ImageIcon icon = new ImageIcon("img/test.jpg");
		System.out.println(icon.getIconWidth() + " " + icon.getIconHeight());

		ImageIcon icon2 = ImageIconUtil.resize(icon, 600,
				ImageIconUtil.YATAYI_ORANLA);
		System.out.println(icon2.getIconWidth() + " " + icon2.getIconHeight());
	}

	public static void main(String[] args) {
		new ImageIconUtilDemo();
	}
}
