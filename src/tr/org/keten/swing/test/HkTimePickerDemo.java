package tr.org.keten.swing.test;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;

import tr.org.keten.swing.components.HkTimePicker;

public class HkTimePickerDemo extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HkTimePicker hkTimePicker;

	public HkTimePickerDemo() {
		setLayout(new GridLayout(4, 1));

		hkTimePicker = new HkTimePicker();
		add(hkTimePicker);

		JButton button = new JButton("Set now time");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				hkTimePicker.setTime(new Date());
			}
		});
		add(button);

		JButton start = new JButton("Timer Start");
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				hkTimePicker.timerStart();
			}
		});
		add(start);

		JButton stop = new JButton("Timer Stop");
		stop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				hkTimePicker.timerStop();
			}
		});
		add(stop);

		pack();
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

	public static void main(String[] args) {
		new HkTimePickerDemo();
	}
}
