package tr.org.keten.swing.components;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import tr.org.keten.swing.util.HkFieldLimit;

public class HkTimePicker extends JPanel implements ChangeListener,
		MouseListener, KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HkTimePicker() {
		init();
	}

	private Times saat;
	private Times dakika;
	private Times saniye;
	private JPopupMenu popup;

	private Slider saatSlider;
	private Slider dakikaSlider;
	private Slider saniyeSlider;

	private String separatorString = ":";

	private void init() {
		setLayout(new BorderLayout());
		setBorder(new JTextField().getBorder());

		add(initJPanel());

		setTime(new Date());
	}

	private static Separator separator;

	private JPanel initJPanel() {
		JPanel panel = new JPanel(new GridLayout());
		panel.setOpaque(false);

		JPanel saatJPanel = new JPanel(new BorderLayout());

		saat = new Times();
		saat.addMouseListener(this);
		saat.addKeyListener(this);
		saatJPanel.add(saat);

		separator = new Separator(separatorString, JLabel.CENTER);
		separator.addMouseListener(this);

		saatJPanel.add(separator, BorderLayout.EAST);
		panel.add(saatJPanel);

		JPanel dakikaJPanel = new JPanel(new BorderLayout());

		dakika = new Times();
		dakika.addMouseListener(this);
		dakika.addKeyListener(this);
		dakikaJPanel.add(dakika);

		separator = new Separator(separatorString, JLabel.CENTER);
		separator.addMouseListener(this);
		dakikaJPanel.add(separator, BorderLayout.EAST);

		panel.add(dakikaJPanel);

		JPanel saniyeJPanel = new JPanel(new BorderLayout());

		saniye = new Times();
		saniye.addMouseListener(this);
		saniye.addKeyListener(this);
		saniyeJPanel.add(saniye);

		panel.add(saniyeJPanel);

		popup = new JPopupMenu();
		JPanel popupJPanel = new JPanel();

		saatSlider = new Slider(Slider.SAAT);
		saatSlider.addChangeListener(this);
		popupJPanel.add(saatSlider);

		dakikaSlider = new Slider(Slider.DAKIKA);
		dakikaSlider.addChangeListener(this);
		popupJPanel.add(dakikaSlider);

		saniyeSlider = new Slider(Slider.SANIYE);
		saniyeSlider.addChangeListener(this);
		popupJPanel.add(saniyeSlider);

		popup.add(popupJPanel);

		return panel;
	}

	public String getTime() {
		return getSaat() + separatorString + getDakika() + separatorString
				+ getSaniye();
	}

	private int saatValue;
	private int dakikaValue;
	private int saniyeValue;

	public void setTime(Date date) {
		if (date == null) {
			throw new NullPointerException();
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		saatValue = calendar.get(Calendar.HOUR_OF_DAY);
		dakikaValue = calendar.get(Calendar.MINUTE);
		saniyeValue = calendar.get(Calendar.SECOND);

		alanlariDoldur(saatValue, dakikaValue, saniyeValue);
	}

	private void alanlariDoldur(int saat, int dakika, int saniye) {
		setSaat(saat);
		setDakika(dakika);
		setSaniye(saniye);
	}

	private String getSaat() {
		return saat.getText();
	}

	private void setSaat(int saat) {
		String s;
		saatSlider.setValue(saat);
		if (saat < 10) {
			s = "0" + saatSlider.getValue();
		} else {
			s = "" + saatSlider.getValue();
		}
		this.saat.setText(s);
		saatSlider.setBorder(BorderFactory.createTitledBorder(s));
	}

	private String getDakika() {
		return dakika.getText();
	}

	private void setDakika(int dakika) {
		String s;
		dakikaSlider.setValue(dakika);
		if (dakika < 10) {
			s = "0" + dakikaSlider.getValue();
		} else {
			s = "" + dakikaSlider.getValue();
		}
		this.dakika.setText(s);
		dakikaSlider.setBorder(BorderFactory.createTitledBorder(s));
	}

	private String getSaniye() {
		return saniye.getText();
	}

	private void setSaniye(int saniye) {
		String s;
		saniyeSlider.setValue(saniye);
		if (saniye < 10) {
			s = "0" + saniyeSlider.getValue();
		} else {
			s = "" + saniyeSlider.getValue();
		}
		this.saniye.setText(s);
		saniyeSlider.setBorder(BorderFactory.createTitledBorder(s));
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() == saatSlider) {
			saatValue = saatSlider.getValue();
			setSaat(saatValue);
		} else if (e.getSource() == dakikaSlider) {
			dakikaValue = dakikaSlider.getValue();
			setDakika(dakikaValue);
		} else if (e.getSource() == saniyeSlider) {
			saniyeValue = saniyeSlider.getValue();
			setSaniye(saniyeValue);
		}
	}

	public void mousePressed(MouseEvent e) {
		popup.show(saat, 0, saat.getSize().height);
	}

	public void keyReleased(KeyEvent e) {
		if (saat.getInt() > 23) {
			setSaat(saat.getInt() % 24);
		}

		if (dakika.getInt() > 59) {
			setDakika(dakika.getInt() % 60);
		}

		if (saniye.getInt() > 59) {
			setSaniye(saniye.getInt() % 60);
		}
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void keyPressed(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	private int iSaat;
	private int iDakika;
	private int iSaniye;
	private Timer timer;

	public void timerStart() {
		timer = new Timer(1000, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				iSaat = Integer.parseInt(getSaat());
				iDakika = Integer.parseInt(getDakika());
				iSaniye = Integer.parseInt(getSaniye());

				iSaniye++;
				if (iSaniye == 60) {
					iSaniye = 0;

					iDakika++;
					if (iDakika == 60) {
						iDakika = 0;

						iSaat++;
						if (iSaat == 24) {
							iSaat = 0;
						}
					}
				}

				alanlariDoldur(iSaat, iDakika, iSaniye);

			}
		});

		timer.start();
	}

	public void timerStop() {
		timer.stop();
	}
}

class Times extends HkSayiField {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Times() {
		setBorder(BorderFactory.createEmptyBorder());
		setHorizontalAlignment(JTextField.CENTER);
		setDocument(new HkFieldLimit(2));
		setColumns(3);
		setText("00");
	}
}

class Separator extends JLabel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Separator(String separator, int center) {
		setText(separator);
		setAlignmentX(center);
		setBackground(new Times().getBackground());
		setForeground(new Times().getForeground());
		setOpaque(true);
	}
}

class Slider extends JSlider {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	static int SAAT = 0;
	static int DAKIKA = 1;
	static int SANIYE = 2;

	public Slider(int i) {
		setBorder(BorderFactory.createTitledBorder("00"));
		setOrientation(JSlider.VERTICAL);
		setMinimum(0);
		if (i == SAAT) {
			setMaximum(23);
			setMajorTickSpacing(3);
		} else {
			setMaximum(59);
			setMajorTickSpacing(5);
		}
		setValue(0);
		setIgnoreRepaint(true);
		setPaintTicks(true);
		setPaintLabels(true);
	}
}