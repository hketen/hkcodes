package tr.org.keten.swing.components;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class HkTabbedPane extends JTabbedPane {

	/**
	 * @author H_K
	 */
	private static final long serialVersionUID = 1L;

	public HkTabbedPane() {
	}

	public HkTabbedPane(int tabPlacement, int tabLayoutPolicy) {
		setTabPlacement(tabPlacement);
		setTabLayoutPolicy(tabLayoutPolicy);
	}

	/**
	 * addClosableTab("String title, Component icerik, ImageIcon closeIcon);
	 * 
	 * örnek: addClosableTab("Baslik", icerikJPanel, new
	 * ImageIcon("close.gif"));
	 * */
	public void addClosableTab(String title, Component icerik,
			ImageIcon closeIcon) {

		addTab(null, icerik);

		if (icerik == null || closeIcon == null) {
			try {
				throw new Exception();
			} catch (Exception e1) {
				System.err.println("icerik or closeIcon is null");
			}
		}

		JPanel titlePane = new JPanel();
		titlePane.setOpaque(false);

		JLabel tabClose = new JLabel(closeIcon);
		tabClose.setToolTipText("Close");

		tabClose.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {

				for (int i = 0; i < getTabCount(); i++) {
					JPanel panel = (JPanel) getTabComponentAt(i);
					JLabel label = (JLabel) panel.getComponent(1);
					if (e.getSource().equals(label)) {
						removeTabAt(i);
						break;
					}
				}

			}
		});

		titlePane.add(new JLabel(title));
		titlePane.add(tabClose);

		setTabComponentAt(getTabCount() - 1, titlePane);
	}

	public void removeTab(String string) {
		this.removeTabAt(this.indexOfTab(string));
	}

}