package tr.org.keten.swing.components;

import java.awt.Color;

import javax.swing.JLabel;

public class HkRenkKupu extends JLabel {

	/**
	 * @author H_K
	 */
	private static final long serialVersionUID = 1L;

	private Color bgColor = new Color(125, 125, 125);
	private Color fgColor = bgColor;
	private boolean opaque = true;

	public HkRenkKupu() {
		init();
	}

	public HkRenkKupu(Color color) {
		bgColor = color;
		fgColor = color;

		init();
	}

	public HkRenkKupu(Color bgColor, Color fgColor) {
		this.bgColor = bgColor;
		this.fgColor = fgColor;

		init();
	}

	private void init() {
		setText("xx");
		setOpaque(opaque);

		setBackground(bgColor);
		setForeground(fgColor);
	}

	public void setColor(Color color) {
		bgColor = color;
		fgColor = color;
		
		init();
	}

	public void setColor(Color bgColor, Color fgColor) {
		this.bgColor = bgColor;
		this.fgColor = fgColor;
		
		init();
	}

	public Color getBgColor() {
		return bgColor;
	}

	public Color getFgColor() {
		return fgColor;
	}

}
