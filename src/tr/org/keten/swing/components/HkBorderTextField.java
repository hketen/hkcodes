package tr.org.keten.swing.components;

import javax.swing.BorderFactory;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class HkBorderTextField extends JTextField{
	
	public HkBorderTextField(String border) {
		setBorder(BorderFactory.createTitledBorder(border));
	}
	
	public HkBorderTextField(String border, int columns){
		setBorder(BorderFactory.createTitledBorder(border));
		setColumns(columns);
	}
}
