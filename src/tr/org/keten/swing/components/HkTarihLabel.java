package tr.org.keten.swing.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.Timer;

public class HkTarihLabel extends JLabel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String date;

	private String getDate() {
		return date;
	}

	private void setDate(String date) {
		this.date = date;
	}

	public HkTarihLabel() {
		addNotify();
	}

	public void addNotify() {
		super.addNotify();

		Timer timer = new Timer(1000, new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setDate(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
				setText(getDate());
			}
		});
		timer.start();
	}
}
