package tr.org.keten.swing.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.Timer;

public class HkSaatLabel extends JLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String time;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public HkSaatLabel() {
		addNotify();
	}

	public void addNotify() {
		super.addNotify();

		Timer timer = new Timer(1000, new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setTime(new SimpleDateFormat("hh:mm:ss").format(new Date()));
				setText(getTime());
			}
		});
		timer.start();
	}
}
