package tr.org.keten.swing.components;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextField;
import javax.swing.Timer;

public class HkSayiField extends JTextField implements KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public final static int BOS_DEGER = 0;

	private final Color alertColor = Color.YELLOW;
	private final Color bgColor = getBackground();

	public HkSayiField() {
		addKeyListener(this);
	}

	public HkSayiField(int columns) {
		setColumns(columns);
		addKeyListener(this);
	}

	@Override
	public void addNotify() {
		super.addNotify();

		Timer timer = new Timer(3000, new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!isEditable()) {
					setEditable(true);
					setBackground(bgColor);
				}
			}
		});
		timer.start();
	}

	public void setInt(int i) {
		if (i != BOS_DEGER) {
			setText(String.valueOf(i));
		} else {
			setText("");
		}
	}

	public void setInt(Long l) {
		if (l != BOS_DEGER) {
			setText(String.valueOf(l));
		} else {
			setText("");
		}
	}

	public int getInt() {
		int ret = BOS_DEGER;

		if (!getText().equalsIgnoreCase("")) {
			try {
				ret = Integer.parseInt(getText());
			} catch (Exception e) {
				new Exception("Maximum int değerini geçemez");
			}
		}

		return ret;
	}

	public long getLong() {
		long ret = BOS_DEGER;

		if (!getText().equalsIgnoreCase("")) {
			try {
				ret = Long.parseLong(getText());
			} catch (Exception e) {
				new Exception("Maximum int değerini geçemez");
			}
		}

		return ret;
	}

	public void keyPressed(KeyEvent e) {
		if (e.getKeyChar() >= '0' && e.getKeyChar() <= '9'
				|| e.getKeyCode() == KeyEvent.VK_BACK_SPACE
				|| e.getKeyCode() == KeyEvent.VK_CAPS_LOCK
				|| e.getKeyCode() == KeyEvent.VK_CONTROL
				|| e.getKeyCode() == KeyEvent.VK_SHIFT
				|| e.getKeyCode() == KeyEvent.VK_ALT
				|| e.getKeyCode() == KeyEvent.VK_ALT_GRAPH
				|| e.getKeyCode() == KeyEvent.VK_TAB
				|| e.getKeyCode() == KeyEvent.VK_DELETE
				|| e.getKeyCode() == KeyEvent.VK_RIGHT
				|| e.getKeyCode() == KeyEvent.VK_LEFT
				|| e.getKeyCode() == KeyEvent.VK_UP
				|| e.getKeyCode() == KeyEvent.VK_DOWN
				|| e.getKeyCode() == KeyEvent.VK_ENTER
				|| e.getKeyCode() == KeyEvent.VK_ESCAPE) {

			setEditable(true);
			setBackground(bgColor);
		} else {
			setEditable(false);
			setBackground(alertColor);
		}
	}

	public void keyTyped(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
	}
}
