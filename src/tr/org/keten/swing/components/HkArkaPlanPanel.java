package tr.org.keten.swing.components;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.LayoutManager;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class HkArkaPlanPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String url;

	private int x1Size = 0;
	private int y1Size = 0;

	private int x2Size = getSize().width;
	private int y2Size = getSize().height;

	private boolean autoSize = false;

	public HkArkaPlanPanel(String url, boolean autoSize) {
		this.url = url;
		this.autoSize = autoSize;
	}

	public HkArkaPlanPanel(LayoutManager l, String url, boolean autoSize) {
		this.url = url;
		
		this.autoSize = autoSize;

		super.setLayout(l);
	}

	public HkArkaPlanPanel(String url, int x2Size, int y2Size) {
		this.url = url;

		this.x2Size = x2Size;
		this.y2Size = y2Size;
	}

	public HkArkaPlanPanel(String url, LayoutManager l, int x2Size, int y2Size) {
		this.url = url;

		this.x2Size = x2Size;
		this.y2Size = y2Size;

		super.setLayout(l);
	}

	public HkArkaPlanPanel(String url, int x1Size, int y1Size, int x2Size,
			int y2Size) {
		this.url = url;

		this.x1Size = x1Size;
		this.y1Size = y1Size;

		this.x2Size = x2Size;
		this.y2Size = y2Size;
	}

	public HkArkaPlanPanel(String url, LayoutManager l, int x1Size, int y1Size,
			int x2Size, int y2Size) {
		this.url = url;

		this.x1Size = x1Size;
		this.y1Size = y1Size;

		this.x2Size = x2Size;
		this.y2Size = y2Size;

		super.setLayout(l);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		if (  (x2Size == 0 && y2Size == 0) || autoSize ) {
			x2Size = getSize().width;
			y2Size = getSize().height;
		}

		Image image = new ImageIcon(url).getImage();

		g.drawImage(image, x1Size, y1Size, x2Size, y2Size, null);
	}

}
