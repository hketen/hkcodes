package tr.org.keten.swing.components;

import javax.swing.BorderFactory;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class HkBorderTextArea extends JTextArea{

	public HkBorderTextArea(String border) {
		setBorder(BorderFactory.createTitledBorder(border));
	}
	
	public HkBorderTextArea(String border, int satir, int sutun){
		setBorder(BorderFactory.createTitledBorder(border));
		setRows(satir);
		setColumns(sutun);
	}
	
	public HkBorderTextArea(String border, String text, int satir, int sutun){
		setBorder(BorderFactory.createTitledBorder(border));
		setRows(satir);
		setColumns(sutun);
		setText(text);
	}
	
	public HkBorderTextArea(String border, int satirSutun){
		setBorder(BorderFactory.createTitledBorder(border));
		setRows(satirSutun);
		setColumns(satirSutun);
	}
	
	public HkBorderTextArea(String border, String text, int satirSutun){
		setBorder(BorderFactory.createTitledBorder(border));
		setRows(satirSutun);
		setColumns(satirSutun);
		setText(text);
	}
	
}
