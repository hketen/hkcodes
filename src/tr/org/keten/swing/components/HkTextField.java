package tr.org.keten.swing.components;

import java.awt.Graphics;

import javax.swing.JTextField;
import javax.swing.text.Document;

public class HkTextField extends JTextField {

	/**
	 * @author Hakan KETEN
	 */
	private static final long serialVersionUID = 1L;

	private String label;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public HkTextField() {
		super();
	}

	public HkTextField(Document doc, String text, int columns) {
		super(doc, text, columns);
	}

	public HkTextField(int columns) {
		super(columns);
	}

	public HkTextField(String text, int columns) {
		super(text, columns);
	}

	public HkTextField(String label) {
		this.label = label;
	}

	public HkTextField(String label, String text, int columns) {
		super(text, columns);
		this.label = label;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (this.getText().equalsIgnoreCase("")) {
			g.drawString(label, 15, (getSize().height / 2) + 5);
		} else {
			g.drawString("", 15, (getSize().height / 2) + 5);
		}
	}
}
