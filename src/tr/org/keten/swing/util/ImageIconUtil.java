package tr.org.keten.swing.util;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

public class ImageIconUtil {

	public static ImageIcon resize(ImageIcon icon, int newWidth, int newHeight) {
		Image img = icon.getImage();

		BufferedImage bi = new BufferedImage(newWidth, newHeight,
				BufferedImage.TYPE_INT_ARGB);
		Graphics g = bi.createGraphics();
		g.drawImage(img, 0, 0, newWidth, newHeight, null);

		return new ImageIcon(bi);
	}

	public static double YATAYI_ORANLA = 0;
	public static double DIKEYI_ORANLA = 1;

	public static ImageIcon resize(ImageIcon icon, int size, double oranti) {
		Image img = icon.getImage();

		int newWidth = 0;
		int newHeight = 0;
		if (oranti == YATAYI_ORANLA) {
			double oran = (double) icon.getIconWidth()
					/ (double) icon.getIconHeight();

			newHeight = size;
			newWidth = (int) (size * oran);
		} else {
			double oran = (double) icon.getIconHeight()
					/ (double) icon.getIconWidth();

			newWidth = size;
			newHeight = (int) ((double) size * oran);
		}

		BufferedImage bi = new BufferedImage(newWidth, newHeight,
				BufferedImage.TYPE_INT_ARGB);
		Graphics g = bi.createGraphics();
		g.drawImage(img, 0, 0, newWidth, newHeight, null);

		return new ImageIcon(bi);
	}
}
