package tr.org.keten.swing.util;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;

public class HkWindowUtil {

	public static void initCloseListener(final JDialog dialog, KeyStroke stroke) {
		JRootPane window = dialog.getRootPane();

		window.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(stroke,
				"close");

		window.getActionMap().put("close", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				dialog.dispose();
			}
		});
	}

}
